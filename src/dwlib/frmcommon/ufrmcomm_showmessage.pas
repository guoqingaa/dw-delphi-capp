unit ufrmcomm_showmessage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, UpPopupMenu, frxClass, frxDesgn,
  frxDBSet, UpDataSource, DB, ADODB, UpAdoTable, UpAdoQuery, Buttons,
  UpSpeedButton, ExtCtrls, UPanel, StdCtrls, UpLabel, UpImage;

type
  Tfrmcomm_showmessage = class(tform)
    pfrmtop: TUPanel;
    btnok: TUpSpeedButton;
    btncancel: TUpSpeedButton;
    bvl1: TBevel;
    lbltext: TUpLabel;
    img1: TUpImage;
    procedure btnokClick(Sender: TObject);
    procedure btncancelClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private

  public
    procedure setmessage(strcaption, strtext : string; btncount : integer);

  end;

var
  frmcomm_showmessage: Tfrmcomm_showmessage;

implementation
           
{$R *.dfm}

procedure Tfrmcomm_showmessage.setmessage(strcaption, strtext: string; btncount : integer);
begin
  self.caption := strcaption;
  lbltext.Caption := strtext;

  //根据按钮个数调整位置
  if btncount = 1 then
  begin
    btncancel.Visible := False;
    btnok.Left := pfrmtop.Width - btnok.Width - 8;
  end
  else if btncount > 1 then
  begin
    //暂时不考虑mrRetry等的情况
    btncancel.Visible := True;
    btncancel.Left := pfrmtop.Width - btncancel.Width - 8;
    btnok.Left := btncancel.Left - 4 - btnok.Width;
  end;
end;

procedure Tfrmcomm_showmessage.btnokClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure Tfrmcomm_showmessage.btncancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure Tfrmcomm_showmessage.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  //回车键--->OK按钮
  if Key = #13 then
    btnokClick(Sender);
  //空格键--->OK按钮
  if Key = #32 then
    btnokClick(Sender);

  //ESC--->Cancel按钮
  if Key = #27 then
    btncancelClick(Sender);
end;

end.

