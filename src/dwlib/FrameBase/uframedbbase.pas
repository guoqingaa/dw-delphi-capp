//*******************************************************************
//  pmyhisgroup  pyijgl   
//  uframedbbase.pas     
//                                          
//  创建: changym by 2012-05-14 
//        changup@qq.com                    
//  功能说明:                                            
//    带数据集控件的frame;
//    注意: 调用前必须先setdllparam(...)设置数据库连接;
//  修改历史:
//
//  版权所有 (C) 2012 by changym
//
//*******************************************************************
unit uframedbbase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, uframeBase, utypes, UpAdoStoreProc, UpAdoTable, DB,
  UpAdoQuery, udlltransfer, udmtbl_public, UpWWDbGrid;

type
  Tframedbbase = class(Tframebase)
    qry: TUpAdoQuery;
    qry1: TUpAdoQuery;
    tbl: TUpAdoTable;
    sp: TUpAdoStoreProc;
    qry2: TUpAdoQuery;
    qry3: TUpAdoQuery;
  public
    fpdllparams : PDllParam;
    dmtbl_public : Tdmtbl_public;

  public
    procedure init(pdllparams : PDllParam); virtual;
    //初始化;
    function initcontrols() : Boolean; virtual;
    //初始化控件,主要用于反复多次的初始化调用

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  protected

  end;

var
  framedbbase: Tframedbbase;

implementation

{$R *.dfm}

{ Tframedbbase }

constructor Tframedbbase.Create(AOwner: TComponent);
begin
  inherited;

  fpdllparams := nil;
end;

destructor Tframedbbase.Destroy;
begin
  FreeAndNil(dmtbl_public);
  inherited;
end;

procedure Tframedbbase.init(pdllparams : PDllParam);
var
  i : integer;
begin
  fpdllparams := pdllparams;
  
  { 窗体数据集connection属性设置
  }
  for i:=0 to self.ComponentCount-1 do
  begin
    //TAdoQuery
    if UpperCase(Self.Components[i].ClassName) = 'TUPADOQUERY' then
    begin
      (self.Components[i] as TADOQuery).Connection := fpdllparams^.padoconn^;
    end;
    //TAdoTable
    if UpperCase(self.Components[i].ClassName) = 'TUPADOTABLE' then
    begin
      (Self.Components[i] as TADOTable).Connection := fpdllparams^.padoconn^;
    end;
    //TADOStoredProc
    if UpperCase(self.Components[i].ClassName) = 'TUPADOSTOREPROC' then
    begin
      (Self.Components[i] as TADOStoredProc).Connection := fpdllparams^.padoconn^;
    end;

    if UpperCase(self.Components[i].ClassName) = 'TUpWWDbGrid' then
    begin
      (Self.Components[i] as TUpWWDbGrid).PaintOptions.AlternatingRowColor :=
         StringToColor(fpdllparams^.mysystem^.global_sysparams.valueitem('dbg_AlternatingRowColor'));
    end;
  end;

  { 创建dmtbl_public对象
  }
  dmtbl_public := Tdmtbl_public.Create(fpdllparams);
end;

function Tframedbbase.initcontrols : Boolean;
begin
  Result := False;
  
  if fpdllparams = nil then
  begin
    errcode := -1;
    errmsg := '请先调用setdllparam';
    Exit;
  end;

  Result := True;
end;

end.
