unit ufrmbumMDI;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbtreebase_mdi, ImgList, frxExportXLS, frxClass,
  frxExportPDF, Menus, UpPopupMenu, frxDesgn, frxDBSet, UpDataSource, DB,
  ADODB, UpAdoTable, UpAdoQuery, ExtCtrls, CnEdit, UpCnedit, StdCtrls,
  Buttons, UpSpeedButton, ToolPanels, UpAdvToolPanel, Grids, Wwdbigrd,
  Wwdbgrid, UpWWDbGrid, ComCtrls, UPanel, RzButton, RzPanel, Mask, RzEdit,
  RzSpnEdt, UpRzspinedit, UpLabel, UpCheckBox;

type
  TfrmbumMDI = class(TfrmdbtreebaseMDI)
    dsbumlingd: TUpDataSource;
    qrybumlingd: TUpAdoQuery;
    updvtlpnl1: TUpAdvToolPanel;
    upnl1: TUPanel;
    dbgbumlingd: TUpWWDbGrid;
    btnaddzhuanj: TUpSpeedButton;
    btndelzhuanj: TUpSpeedButton;
    chkcanjps: TUpCheckBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tvtreeChange(Sender: TObject; Node: TTreeNode);

  private
    procedure refreshdataset_bunlingd(bumbm: integer);
    //刷新部门领导

  protected
    function getnodename_showtreenode() : string; override;

    function addrootnodecheck() : Boolean; override;
    //添加根节点验证
    function addchildnodecheck() : Boolean; override;
    //添加子节点验证
    function UpdateNodeCheck() : Boolean; override;
    //修改节点验证

    function deletecheck() : Boolean; override;

    function AddRootNode_SetFields() : Boolean; override;
    //添加根节点设置字段值
    function AddChildNode_Setfields(fujdbm : integer) : Boolean; override;
    //添加子节点设置字段值
    function UpdateNode_SetFields(fujdbm : integer) : Boolean; override;
    //修改节点设置字段值

    function UpdateNodeBefore() : Boolean; override;
    //修改节点前
    function UpdateNodeAfter() : Boolean; override;
    //修改节点成功后,主要用于变动tree的显示
    
    function Execute_dll_show_before() : Boolean; override;  
  end;

var
  frmbumMDI: TfrmbumMDI;

implementation

uses ufrmcomm_selyuang, ufrmdbbase,
  udbgselfdraw_dbgid_define, ufrmcomm_selbumyuang;

{$R *.dfm}

function TfrmbumMDI.AddChildNode_Setfields(fujdbm: integer): Boolean;
begin
  //增加用户节点赋值
  qrytree.FieldByName('yonghbm').AsString := edtyonghbm.Text;
  qrytree.FieldByName('paixh').Value := edtpaixh.Value;
  if chkcanjps.Checked then
    qrytree.FieldByName('canjpsbs').AsInteger := 1
  else
    qrytree.FieldByName('canjpsbs').AsInteger := 0;

  Result := inherited AddChildNode_Setfields(fujdbm);
end;

function TfrmbumMDI.addchildnodecheck: Boolean;
begin
  Result := inherited addchildnodecheck();
end;

function TfrmbumMDI.AddRootNode_SetFields: Boolean;
begin
  //增加用户节点赋值                   
  qrytree.FieldByName('yonghbm').AsString := edtyonghbm.Text;
  qrytree.FieldByName('paixh').Value := edtpaixh.Value;
  if chkcanjps.Checked then
    qrytree.FieldByName('canjpsbs').AsInteger := 1
  else
    qrytree.FieldByName('canjpsbs').AsInteger := 0;
  
  Result := inherited AddRootNode_SetFields;
end;

function TfrmbumMDI.addrootnodecheck: Boolean;
begin
  Result := inherited addrootnodecheck;
end;

function TfrmbumMDI.deletecheck: Boolean;
begin
  //删除前验证检查
  Result := False;
  
  //1. 是否存在下级节点
  if dbcheck.RowIsExists('select top 1 * from jc_bum where fujdbm=' +
      qrytree.fieldbyname('bianm').AsString) then
  begin
    baseobj.showwarning('当前节点存在下级引用,不允许删除！');
    Exit;
  end;

  //2. 是否存在<业务表>的引用
  {
  if dbcheck.RowIsExists('select top 1 * from jc_wuz where fenlbm=' +
      qrytree.fieldbyname('bianm').AsString) then
  begin
    baseobj.showwarning('当前分类存在物资明细,不允许删除！');
    Exit;
  end;
  }

  Result := True;
end;

function TfrmbumMDI.Execute_dll_show_before: Boolean;
begin
  tablename := 'jc_bum';
  wherestr:= ' where zhuangt=1 ';

  result := inherited Execute_dll_show_before;
end;

procedure TfrmbumMDI.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;

  frmbumMDI := nil;
end;

function TfrmbumMDI.getnodename_showtreenode: string;
var
  yonghbm : string;
begin
  Result := qrytree.fieldbyname('mingc').AsString
  {
  yonghbm := qrytree.fieldbyname('yonghbm').AsString;
  if Trim(yonghbm) = '' then
    Result := qrytree.fieldbyname('mingc').AsString
  else
    Result := qrytree.fieldbyname('yonghbm').AsString +
      ' ' + qrytree.fieldbyname('mingc').AsString;
  }
end;

function TfrmbumMDI.UpdateNode_SetFields(fujdbm: integer): Boolean;
begin
  //增加用户节点赋值                   
  qrytree.FieldByName('yonghbm').AsString := edtyonghbm.Text;
  qrytree.FieldByName('paixh').Value := edtpaixh.Value;
  if chkcanjps.Checked then
    qrytree.FieldByName('canjpsbs').AsInteger := 1
  else
    qrytree.FieldByName('canjpsbs').AsInteger := 0;
  
  Result := inherited UpdateNode_SetFields(fujdbm);
end;

function TfrmbumMDI.UpdateNodeAfter: Boolean;
var
  fenlbmqm : string;
  fenlmcqm : string;
begin
  {
  if Trim(edtyonghbm.Text) <> '' then
    tvtree.Selected.Text := edtyonghbm.Text + ' ' + edtmc.Text
  else
    tvtree.Selected.Text := edtmc.Text;
  }
  tvtree.Selected.Text := edtmc.Text;

  {修复jc_wuz表分类全名字段
  qry2.OpenSql('select bianm,fenlbm from jc_wuz ' +
      ' where fenlbmqm like ''%/' + qrytree.fieldbyname('bianm').AsString +
      '/%''');
  qry2.First;
  while not qry2.Eof do
  begin
    fenlbmqm := getnodefullbm(qry2.getInteger('fenlbm')) + '/';
    fenlmcqm := getnodefullname(qry2.getInteger('fenlbm'));

    qry1.ExecuteSql('update jc_wuz set fenlbmqm=''' + fenlbmqm +
        ''', fenlmcqm=''' + fenlmcqm + '''' +
        ' where bianm=' + qry2.getString('bianm'));

    qry2.Next;
  end;
  qry2.Close; 
  }
end;

function TfrmbumMDI.UpdateNodeBefore: Boolean;
begin
  Result := False;
  if not inherited UpdateNodeBefore then Exit;

  edtyonghbm.Text := qrytree.fieldbyname('yonghbm').AsString;  
  edtmc.text := qrytree.fieldbyname('mingc').asstring;
  edtpaixh.Value := qrytree.getInteger('paixh');
  chkcanjps.Checked:= qrytree.getInteger('canjpsbs')=1;
  
  Result := True;
end;

function TfrmbumMDI.UpdateNodeCheck: Boolean;
begin
  Result := inherited UpdateNodeCheck;
end;

procedure TfrmbumMDI.refreshdataset_bunlingd(bumbm: integer);
begin   
  qrybumlingd.OpenSql(Format('select * from v_yuang' +
    ' where bianm in(select yuangbm from jc_bum_shenpr' +
    ' where bumbm=%d)', [bumbm]));
end;

procedure TfrmbumMDI.tvtreeChange(Sender: TObject; Node: TTreeNode);
begin
  inherited;

  if qrytree.isEmpty then
  begin
    refreshdataset_bunlingd(0);
  end
  else
  begin
    refreshdataset_bunlingd(qrytree.getInteger('bianm'));
  end;
end;

end.
