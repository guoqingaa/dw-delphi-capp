inherited frmxitgl_menzyspb: Tfrmxitgl_menzyspb
  Left = 69
  Top = 64
  Caption = #38376#35786#21307#29983#25490#29677
  ClientHeight = 469
  ClientWidth = 1149
  PixelsPerInch = 96
  TextHeight = 15
  inherited pbuttons: TPanel
    Width = 1149
    inherited btnadd: TUpSpeedButton
      Left = 678
      Top = 4
      Visible = False
    end
    inherited btnupdate: TUpSpeedButton
      Visible = False
    end
    inherited btndelete: TUpSpeedButton
      Visible = False
    end
    inherited btncancel: TUpSpeedButton
      Left = 6
      alignleftControl = nil
    end
    inherited btnsave: TUpSpeedButton
      Left = 296
    end
    inherited btnrefresh: TUpSpeedButton
      Left = 402
    end
  end
  inherited dbg: TUpWWDbGrid
    Top = 68
    Width = 1149
    Height = 401
    ControlType.Strings = (
      'w1am;CheckBox;1;0'
      'w1pm;CheckBox;1;0'
      'w1em;CheckBox;1;0'
      'w2am;CheckBox;1;0'
      'w2pm;CheckBox;1;0'
      'w2em;CheckBox;1;0'
      'w3am;CheckBox;1;0'
      'w3pm;CheckBox;1;0'
      'w3em;CheckBox;1;0'
      'w4am;CheckBox;1;0'
      'w4pm;CheckBox;1;0'
      'w4em;CheckBox;1;0'
      'w5am;CheckBox;1;0'
      'w5pm;CheckBox;1;0'
      'w5em;CheckBox;1;0'
      'w6am;CheckBox;1;0'
      'w6pm;CheckBox;1;0'
      'w6em;CheckBox;1;0'
      'w7am;CheckBox;1;0'
      'w7pm;CheckBox;1;0'
      'w7em;CheckBox;1;0')
    Selected.Strings = (
      'bianm'#9'6'#9#32534#30721#9'T'
      'yisxm'#9'10'#9#21307#29983#9'T'
      'w1am'#9'6'#9#19978#21320#9'F'#9#21608#19968
      'w1pm'#9'6'#9#19979#21320#9'F'#9#21608#19968
      'w1em'#9'6'#9#22812#29677#9'F'#9#21608#19968
      'w2am'#9'6'#9#19978#21320#9'F'#9#21608#20108
      'w2pm'#9'6'#9#19979#21320#9'F'#9#21608#20108
      'w2em'#9'6'#9#22812#29677#9'F'#9#21608#20108
      'w3am'#9'6'#9#19978#21320#9'F'#9#21608#19977
      'w3pm'#9'6'#9#19979#21320#9'F'#9#21608#19977
      'w3em'#9'6'#9#22812#29677#9'F'#9#21608#19977
      'w4am'#9'6'#9#19978#21320#9'F'#9#21608#22235
      'w4pm'#9'6'#9#19979#21320#9'F'#9#21608#22235
      'w4em'#9'6'#9#22812#29677#9'F'#9#21608#22235
      'w5am'#9'6'#9#19978#21320#9'F'#9#21608#20116
      'w5pm'#9'6'#9#19979#21320#9'F'#9#21608#20116
      'w5em'#9'6'#9#22812#29677#9'F'#9#21608#20116
      'w6am'#9'6'#9#19978#21320#9'F'#9#21608#20845
      'w6pm'#9'6'#9#19979#21320#9'F'#9#21608#20845
      'w6em'#9'6'#9#22812#29677#9'F'#9#21608#20845
      'w7am'#9'6'#9#19978#21320#9'F'#9#21608#26085
      'w7pm'#9'6'#9#19979#21320#9'F'#9#21608#26085
      'w7em'#9'6'#9#22812#29677#9'F'#9#21608#26085)
    FixedCols = 2
    TitleLines = 2
  end
  object ptop: TUPanel [2]
    Left = 0
    Top = 31
    Width = 1149
    Height = 37
    Align = alTop
    ParentColor = True
    TabOrder = 2
    object lblkes: TUpLabel
      Left = 8
      Top = 11
      Width = 83
      Height = 15
      Caption = #35831#36873#25321#31185#23460':'
    end
    object cbbkes: TUpComboBox
      Left = 94
      Top = 8
      Width = 104
      Height = 23
      Style = csDropDownList
      DropDownCount = 28
      ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
      ItemHeight = 15
      TabOrder = 0
      OnChange = cbbkesChange
      AutoInit = False
      HistoryValue = -1
      HistoryValueOnOff = True
    end
  end
  inherited qry: TUpAdoQuery
    Left = 841
    Top = 10
  end
  inherited tbl: TUpAdoTable
    CursorType = ctStatic
    TableName = 'v_menz_yis_paib'
    Left = 233
    Top = 168
  end
  inherited ds: TDataSource
    Left = 265
    Top = 166
  end
end
