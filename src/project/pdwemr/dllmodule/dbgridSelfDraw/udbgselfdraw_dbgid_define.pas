//*******************************************************************
//  pmyhisgroup  pdbgridSelfDraw   
//  udbgselfdraw_dbgid_define.pas     
//                                          
//  创建: changym by 2012-08-03 
//        changup@qq.com                    
//  功能说明:                                            
//      dbg自画模块,各业务窗体dbg标识定义文件
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//
//*******************************************************************
unit udbgselfdraw_dbgid_define;

interface

uses
  Graphics, Windows;

  
{ 颜色常量定义 }
//编辑颜色
const dbg_edit_color = '$ffffff';
//只读颜色
//const dbg_readonly_color = '$00EBEBEB';
const dbg_readonly_color = '$ffffff';
//只读字段颜色 
//const dbg_field_readonly_color = '$00EBEBEB';  
const dbg_field_readonly_color = '$ffffff';
//高亮颜色
//const dbg_highlight_color = '$00FFE0C1';
const dbg_highlight_color = '$00FFD9D7'; 
//金额字段字体颜色
const dbg_moneyfield_forcecolor = '$ff0000';
//金额字段背景颜色
const dbg_moneyfield_bgcolor = '$000000';

//数量字段前景色  
const dbg_numberfield_forcecolor = '$0000FF';
//数量字段背景色
const dbg_numberfield_bgcolor = '$000000';


{ 住院医嘱颜色定义 }   
//医嘱允许编辑;白色
//护士已经审核; 偏绿色;
const clYiz_husYijShenh = '$00D9FFD9';
//护士已经执行; 偏绿色;
const clYiz_husYijZhix = '$00D9FFD9';
//医生已经停嘱; 偏灰色
//护士已计费标识; 偏蓝色;
const clYiz_husYijJif = '$00FFC286';

{ 门诊医生站 }
//门诊医生站-西药处方单-编辑dbg
const dbgselfdraw_menz_yisz_xiycfd_eddbg = 'dbgselfdraw_menz_yisz_xiycfd_eddbg';

{ 住院医生站 }
//住院医生站-医嘱管理-长期医嘱-编辑dbg
const dbgselfdrqw_zhuy_yisz_yizgl_changqyz_eddbg = 'dbgselfdrqw_zhuy_yisz_yizgl_changqyz_eddbg';
//2012-11-22：实际中这个dbg的selfid同长期医嘱，就借用长期医嘱的自画函数了；
const dbgselfdrqw_zhuy_yisz_yizgl_linsyz_eddbg = 'dbgselfdrqw_zhuy_yisz_yizgl_linsyz_eddbg';

{ 住院护士站 }
//住院护士站医嘱审核窗口医嘱dbg
const dbgselfdrgw_zhuy_husz_yizshenh_changqyz = 'dbgselfdrgw_zhuy_husz_yizshenh_changqyz';
//2012-11-22：实际中这个dbg的selfid同长期医嘱，就借用长期医嘱的自画函数了；
const dbgselfdrgw_zhuy_husz_yizshenh_linsyz = 'dbgselfdrgw_zhuy_husz_yizshenh_linsyz';
                                                                           
//住院护士站医嘱管理窗口医嘱dbg
const dbgselfdrgw_zhuy_husz_yizguanli_changqyz = 'dbgselfdrgw_zhuy_husz_yizguanli_changqyz';
//2012-12-16：临时医嘱dbg的selfid同长期医嘱，就借用长期医嘱的自画函数了；



{ 物资库查询报表 }



{ 药库查询报表 }
//出库单查询；主、次dbg；
const dbgselfdrgw_cxbb_yaok_chukd_dbg1 = 'cxbb_yaok_chukd_dbg1';
const dbgselfdrgw_cxbb_yaok_chukd_dbg2 = 'cxbb_yaok_chukd_dbg2';
//出入库汇总表
const dbgselfdrgw_cxbb_yaok_churkhzb_dbg = 'cxbb_yaok_churkhzb_dbg';
//出入库汇总表=按供应商
const dbgselfdrgw_cxbb_yaok_churkhzb_gongys_dbg = 'cxbb_yaok_churkhzb_gongys_dbg';
//出入库汇总表-按药品材料
const dbgselfdrgw_cxbb_yaok_churkhzb_yaopcl_dbg = 'cxbb_yaok_churkhzb_yaopcl_dbg';
//出入库汇总表-按药品类别
const dbgselfdrgw_cxbb_yaok_churkhzb_yaoplb_dbg = 'cxbb_yaok_churkhzb_yaoplb_dbg';
//库存明细表
const dbgselfdrgw_cxbb_yaok_kucmxb_dbg = 'cxbb_yaok_kucmxb_dbg';
//库存明细汇总表
const dbgselfdrgw_cxbb_yaok_kucmxhzb_dbg = 'cxbb_yaok_kucmxhzb_dbg';
//库存明细汇总表-按批号
const dbgselfdrgw_cxbb_yaok_kucmxhzb_pih_dbg = 'cxbb_yaok_kucmxhzb_pih_dbg';
//药品变动明细
const dbgselfdrgw_cxbb_yaok_kucypbdmx_dbg = 'cxbb_yaok_kucypbdmx_dbg';
//药房请领单dbg1、dbg2
const dbgselfdrgw_cxbb_yaok_qingld_dbg1 = 'cxbb_yaok_qingld_dbg1';
const dbgselfdrgw_cxbb_yaok_qingld_dbg2 = 'cxbb_yaok_qingld_dbg2';
//药房入库单dbg1、dbg2
const dbgselfdrgw_cxbb_yaok_rukd_dbg1 = 'cxbb_yaok_rukd_dbg1';
const dbgselfdrgw_cxbb_yaok_rukd_dbg2 = 'cxbb_yaok_rukd_dbg2';
//药房退库单dbg1、dbg2
const dbgselfdrgw_cxbb_yaok_tuikd_dbg1 = 'cxbb_yaok_tuikd_dbg1';
const dbgselfdrgw_cxbb_yaok_tuikd_dbg2 = 'cxbb_yaok_tuikd_dbg2';
//药品出入库明细表
const dbgselfdrgw_cxbb_yaok_yaop_churkmxb_dbg = 'cxbb_yaok_yaop_churkmxb_dbg';  
//药品调价记录dbg1；dbg2；
const dbgselfdrgw_cxbb_yaok_yaoptjjl_dbg1 = 'cxbb_yaok_yaoptjjl_dbg1';
const dbgselfdrgw_cxbb_yaok_yaoptjjl_dbg2 = 'cxbb_yaok_yaoptjjl_dbg2';



{ 药房查询报表 }

{ 系统管理 }
const dbgselfdraw_xitgl_daohlqxdbg = 'dbgselfdraw_xitgl_daohlqxdbg';

{ 兰州市东软医保结算部分 }
const dbgselfdraw_iyblzdr_menzjs_feiydmx = 'dbgselfdraw_iyblzdr_menzjs_feiydmx';
//门诊结算-费用单明细记录
const dbgselfdraw_iyblzdr_zhuyjs_feiydmx = 'dbgselfdraw_iyblzdr_zhuyjs_feiydmx';
//住院结算-费用单明细记录


implementation

end.
